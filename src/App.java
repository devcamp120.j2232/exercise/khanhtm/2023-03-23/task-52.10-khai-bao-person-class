import com.devcampt.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Person person = new Person();
        Person person2 = new Person("Khanh", "Tran", "male", "0000000", "khanhtran@gmail.com");
        System.out.println("Hello, persion1: " + person.toString());
        System.out.println("Hello, persion2: " + person2.toString());
    }
}
