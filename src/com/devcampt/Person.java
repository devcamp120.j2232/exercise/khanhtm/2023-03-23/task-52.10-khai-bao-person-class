package com.devcampt;

public class Person {
  private String firstName;
  private String lastName;
  private String gender;
  private String contact;
  private String email;

  public Person(String firstName, String lastName, String gender, String contact, String email) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
    this.contact = contact;
    this.email = email;
}

  @Override
  public String toString() {
    return "Person [firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender + ", contact=" + contact
        + ", email=" + email + "]";
  }

  public Person() {
  }

  public String getFirstName() {
    return firstName;
  }
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getLastName() {
    return lastName;
  }
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  public String getGender() {
    return gender;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public String getContact() {
    return contact;
  }
  public void setContact(String contact) {
    this.contact = contact;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
}
